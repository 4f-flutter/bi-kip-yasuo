# Bí Kíp Yasuo

Một cẩm nang hướng dẫn chơi không thể thiếu cho những người hâm mộ Yasuo!

## Chức năng chính:

Danh mục **Tổng quan** gồm có:

- Tiểu sử về Yasuo dựa theo website [Universe of League of Legends](https://universe.leagueoflegends.com/)

Các phần dưới đây được dựa theo nội dung sẵn có trên [League of Legends Wiki](https://leagueoflegends.fandom.com/wiki/Yasuo):

- Thông tin cơ bản về Yasuo
- Chỉ số cơ bản của Yasuo trong game.
- Danh mục các trang phục (skin).
- Bộ kĩ năng và video mô tả.

Danh mục **Hướng dẫn** dựa theo guide [Yamikaze & Yeager's Challenger Yasuo Guide ](https://www.mobafire.com/league-of-legends/build/yamikaze-amp-yeagers-challenger-yasuo-guide-501821) gồm có:

- Bảng ngọc bổ trợ khuyên dùng.
- Phép bổ trợ khuyên dùng.
- Trang bị khuyên dùng.
- Thứ tự nâng kĩ nâng.
- Điểm mạnh và điểm yếu.
- Kĩ năng khi đi đường.
- Giao tranh và đẩy lẻ.
- Kĩ thuật nâng cao: Beyblade, Airblade, Keyblade.

Danh mục **Thêm** gồm có:

- Highlight / montage.
- Hình ảnh cosplay.
- Fan arts.
- Thông tin về ứng dụng.

## Yêu cầu để phát triển hoặc triển khai
- [Flutter](https://flutter.dev/) bản mới nhất.
- [Xcode 9.0](https://developer.apple.com/xcode/) hoặc mới hơn.
- [Android Studio](https://developer.android.com/studio), [Genymotion](https://www.genymotion.com/),....
- Thiết bị Android 4.1 (API level 16).
- Thiết bị iOS 64-bit (iPhone 5s trở về sau).

Về cách thức triển khai, vui lòng đọc tại [đây](https://flutter.dev/docs/deployment/android) cho Android và tại [đây](https://flutter.dev/docs/deployment/ios) cho iOS.

Made with ♥ by 4fingers. Feel free to use or make your own clone.
Just remember the [original repo](https://gitlab.com/4f-flutter/bi-kip-yasuo).