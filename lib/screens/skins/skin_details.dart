import 'package:flutter/material.dart';
import '../../styles/box_decorations.dart';
import '../../styles/texts.dart';

class SkinDetailContainer extends StatelessWidget {
  SkinDetailContainer({ this.img, this.detail });

  final String img;
  final String detail;

  Widget build(BuildContext context) => Container(
    child: ListView(
      children: <Widget>[
        Image.asset(img),
        Container(
          alignment: Alignment.center,
          padding: EdgeInsets.all(10.0),
          child: Text(detail, style: textStyles['paragraph']),
          decoration: boxDecorations['paragraph'],
        ),
      ],
    ),
  );
}

class SkinDetails extends StatefulWidget {
  @override
  _SkinDetailsState createState() => _SkinDetailsState();
}

class _SkinDetailsState extends State<SkinDetails> {
  final List<SkinDetailContainer> skinsContainersList = [
    SkinDetailContainer(
      img: 'assets/images/yasuo-classic.jpg',
      detail: 'Yasuo Mặc Định',
    ),
    SkinDetailContainer(
      img: 'assets/images/yasuo-high-noon.jpg',
      detail: 'Yasuo Cao Bồi'
    ),
    SkinDetailContainer(
      img: 'assets/images/yasuo-project.jpg',
      detail: 'Yasuo: Siêu Phẩm',
    ),
    SkinDetailContainer(
      img: 'assets/images/yasuo-blood-moon.jpg',
      detail: 'Yasuo Huyết Nguyệt',
    ),
    SkinDetailContainer(
      img: 'assets/images/yasuo-nightbringer.jpg',
      detail: 'Yasuo Ma Kiếm',
    ),
    SkinDetailContainer(
      img: 'assets/images/yasuo-odyssey.jpg',
      detail: 'Yasuo Kiếm Khách Không Gian',
    )
  ];

  var currentPageValue = 0.0;
  PageController controller = PageController();

  Widget build(BuildContext context) => Scaffold(
    appBar: AppBar(
      title: Text('Skin'),
    ),
    body: PageView.builder(
      controller: controller,
      itemBuilder: (BuildContext bc, int index) => skinsContainersList[index],
      itemCount: skinsContainersList.length,
    ),
  );
}