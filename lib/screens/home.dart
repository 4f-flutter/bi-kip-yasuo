import 'package:flutter/material.dart';
import '../containers/home/general.dart';
import '../containers/home/guide.dart';
import '../containers/home/more.dart';
import '../styles/colors.dart';

class Home extends StatefulWidget {
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  int _index = 0;
  final bottomNavOptions = [
    General(),
    Guide(),
    More()
  ];

  Widget build(BuildContext context) => Scaffold(
    body: SafeArea(
      child: Container(
        child: bottomNavOptions.elementAt(_index)
      ),
    ),
    bottomNavigationBar: BottomNavigationBar(
      items: <BottomNavigationBarItem> [
        BottomNavigationBarItem(icon: Icon(Icons.home), title: Text('Tổng quan'), backgroundColor: Color(0xFF091120)),
        BottomNavigationBarItem(icon: Icon(Icons.note), title: Text('Hướng dẫn'), backgroundColor: colors['cape_palliser']),
        BottomNavigationBarItem(icon: Icon(Icons.more_horiz), title: Text('Thêm'), backgroundColor: colors['wood_smoke']),
      ],
      currentIndex: _index,
      type: BottomNavigationBarType.shifting,
      //fixedColor: colors['firefly'],
      onTap: (index) {setState(() {
        _index = index;
      });},
    ),
  );
}