import 'package:flutter/material.dart';

Map<String, Color> colors = {
  'blue': new Color.fromRGBO(127, 215, 255, 1),
  'ability_name': new Color.fromRGBO(181, 181, 253, 1),
  'cape_palliser': new Color.fromRGBO(147, 115, 65, 1),
  'indian_khaki': new Color.fromRGBO(196, 185, 152, 1),
  'subtitle': new Color.fromRGBO(210, 170, 110, 1),
  'paragraph': new Color.fromRGBO(240, 230, 210, 1),
  'wood_smoke': new Color(0xFF111318),
  'firefly': new Color(0xFF091120),
  'neon_carrot': new Color(0xFFFF9933),
};