import 'package:flutter/material.dart';
import 'colors.dart';

Map<String, TextStyle> textStyles = {
  'ability_name': new TextStyle(
    color: colors['ability_name'],
    fontSize: 18.0,
    fontWeight: FontWeight.bold,
  ),
  'ability_detail': new TextStyle(
    color: Colors.white,
    fontSize: 16.0,
  ),
  'blue_text': new TextStyle(color: colors['blue'], fontWeight: FontWeight.bold,),
  'big_heading': new TextStyle(
    color: colors['cape_palliser'],
    fontSize: 80.0,
  ),
  'heading': new TextStyle( 
    color: colors['cape_palliser'],
    fontSize: 25.0,
    fontWeight: FontWeight.bold,
  ),
  'khaki_heading': new TextStyle(
    color: colors['indian_khaki'],
    fontSize: 16.0,
    fontWeight: FontWeight.bold
  ),
  'neon_carrot_heading': new TextStyle(
    color: colors['neon_carrot'],
    fontSize: 16.0,
    fontWeight: FontWeight.bold
  ),
  'subtitle': new TextStyle(
    color: colors['indian_khaki']
  ),
  'paragraph': new TextStyle(
    color: colors['indian_khaki'],
    fontSize: 16.0
  ),
  'quote': new TextStyle(
    color: colors['indian_khaki'],
    fontSize: 18.0,
    fontWeight: FontWeight.bold,
  ),
  'item_group_name': new TextStyle(
    color: Colors.white,
    fontSize: 16.0,
  ),
  'item_name': new TextStyle(
    color: Colors.white,
  ),
  'link': new TextStyle(
    color: Colors.white,
    fontSize: 16.0,
    fontWeight: FontWeight.bold,
    //height: ,
  ),
};