import 'package:flutter/material.dart';
import 'colors.dart';

Map<String, BoxDecoration> boxDecorations = {
  'paragraph': new BoxDecoration(
    color: colors['wood_smoke'],
  ),
};