import 'package:flutter/material.dart';
import '../guide/runes_and_spells.dart';
import '../guide/recommend_items.dart';
import '../guide/ability_order.dart';
import '../guide/pros_cons.dart';
import '../guide/laning.dart';
import '../guide/team_fighting_and_splitpushing.dart';
import '../guide/advanced_mechanics.dart';

class Guide extends StatelessWidget {

  Widget build(BuildContext context) => Container(
    padding: EdgeInsets.all(10.0),
    color: Color(0xFF091120),
    child: ListView(
      children: <Widget>[
        RunesAndSpells(),
        RecommendItems(),
        AbilityOrders(),
        ProsCons(),
        Laning(),
        TeamfightingAndSplitpushing(),
        AdvancedMechanics(),
      ],
    ),
  );
}