import 'package:flutter/material.dart';
import '../../styles/colors.dart';
import '../more/highlight_montage.dart';
import '../more/cosplay.dart';
import '../more/fan_arts.dart';
import '../more/about.dart';

class More extends StatelessWidget {
  Widget build(BuildContext context) => Container(
    child: DefaultTabController(
      length: 4,
      child: Scaffold(
        appBar: AppBar(
          flexibleSpace: SafeArea(
            child: TabBar(
              tabs: <Widget> [
                Tab(text: 'HIGHLIGHT / MONTAGE'),
                Tab(text: 'COSPLAY'),
                Tab(text: 'FAN ARTS'),
                Tab(text: 'VỀ ỨNG DỤNG',)
              ]
            ),
          ),
          backgroundColor: colors['wood_smoke'],
        ),
        body: TabBarView(
          children: [
            HighlightMontage(),
            Cosplay(),
            FanArts(),
            About(),
          ],
        ),
      ),
    ),
  );
}