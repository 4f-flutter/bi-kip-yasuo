import 'package:flutter/material.dart';
import '../general/base.dart';
import '../general/skins.dart';
import '../general/abilities.dart';

class General extends StatelessWidget {
  Widget build(BuildContext context) => Container(
    padding: EdgeInsets.all(10.0),
    child: ListView(
      children: <Widget>[
        Base(),
        Skins(),
        Abilities(),
      ],
    ),
    decoration: BoxDecoration(
        color: Color(0xFF0A1827),
        
    ),
    width: MediaQuery.of(context).size.width, // device width
  );
}