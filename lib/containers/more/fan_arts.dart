import 'package:flutter/material.dart';
import '../more/open_link_list.dart';
import '../../styles/colors.dart';

class FanArts extends StatelessWidget {
  final List<OpenLinkListItem> fanArtsList = [
    OpenLinkListItem(
      title: 'LoL Wallpapers',
      itemUrl: 'http://www.lol-wallpapers.com/champions/yasuo-fan-art/'
    ),
    OpenLinkListItem(
      title: 'Devian Art',
      itemUrl: 'https://www.deviantart.com/tag/yasuo'
    ),
  ];

  // Build ListView Items
  Widget buildItemsList(BuildContext context, int index, List<OpenLinkListItem> list) => list[index];

  // Build ListView
  ListView buildListView(List<OpenLinkListItem> list) => ListView.builder(
    itemCount: list.length,
    itemBuilder: (BuildContext bc, int i) => buildItemsList(bc, i, list),
    shrinkWrap: true,
    scrollDirection: Axis.vertical,
  );

  Widget build(BuildContext context) => Container(
    child: OpenLinkList (items: buildListView(fanArtsList))
  );
}