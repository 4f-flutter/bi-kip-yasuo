import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import '../../styles/colors.dart';

class About extends StatelessWidget {
  Widget build(BuildContext context) => Container(
    color: colors['firefly'],
    child: Column(
      children: <Widget>[
        Text('Bí Kíp Yasuo', style: TextStyle(color: Colors.white)),
        Text('Một cẩm nang hướng dẫn chơi không thể thiếu cho những người hâm mộ Yasuo!', style: TextStyle(color: colors['cape_palliser']),),
        RaisedButton(
          child: Text("Gitlab"),
          onPressed: _launchGitLab,
        )
      ],
    ),
  );

  _launchGitLab() async {
    const url = "https://gitlab.com/4f-flutter/bi-kip-yasuo";
    if (await canLaunch(url)) {
      await launch(url);
    }
    else {
      throw "Could not launch: $url";
    }
  }
}