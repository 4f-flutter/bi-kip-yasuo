import 'package:flutter/material.dart';
import '../more/open_link_list.dart';
import '../../styles/colors.dart';

class HighlightMontage extends StatelessWidget {
  final List<OpenLinkListItem> highlightList = [
    OpenLinkListItem(
      title: 'Cậu Vàng / Bronze 5 - Boy One Champ Yasuo Việt Nam',
      itemUrl: 'https://www.youtube.com/watch?v=XcK2i5lKsl8'
    ),
    OpenLinkListItem(
      title: 'Tổng Hợp Highlight Yassuo - Best Yasuo NA',
      itemUrl: 'https://www.youtube.com/watch?v=PZ0gmVftsII'
    ),
  ];

  // Build ListView Items
  Widget buildItemsList(BuildContext context, int index, List<OpenLinkListItem> list) => list[index];

  // Build ListView
  ListView buildListView(List<OpenLinkListItem> list) => ListView.builder(
    itemCount: list.length,
    itemBuilder: (BuildContext bc, int i) => buildItemsList(bc, i, list),
    shrinkWrap: true,
    scrollDirection: Axis.vertical,
  );

  Widget build(BuildContext context) => Container(
    child: OpenLinkList (items: buildListView(highlightList))
  );
}