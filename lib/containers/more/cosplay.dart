import 'package:flutter/material.dart';
import '../more/open_link_list.dart';
import '../../styles/colors.dart';

class Cosplay extends StatelessWidget {
  final List<OpenLinkListItem> cosplayList = [
    OpenLinkListItem(
      title: 'Cosplay thầy trò Taliyah và Yasuo',
      itemUrl: 'https://game.thanhnien.vn/tin-tuc/lmht-tuyen-tap-cosplay-cuc-an-tuong-ve-thay-tro-yasuo-va-taliyah-138895.html'
    ),
    OpenLinkListItem(
      title: 'Bất ngờ với cosplay Yasuo cực kỳ oai phong lẫm liệt ngoài đời thực',
      itemUrl: 'http://game8.vn/tin-moi/bat-ngo-voi-cosplay-yasuo-cuc-ky-oai-phong-lam-liet-ngoai-doi-thuc-50931'
    ),
  ];

  // Build ListView Items
  Widget buildItemsList(BuildContext context, int index, List<OpenLinkListItem> list) => list[index];

  // Build ListView
  ListView buildListView(List<OpenLinkListItem> list) => ListView.builder(
    itemCount: list.length,
    itemBuilder: (BuildContext bc, int i) => buildItemsList(bc, i, list),
    shrinkWrap: true,
    scrollDirection: Axis.vertical,
  );

  Widget build(BuildContext context) => Container(
    child: OpenLinkList (items: buildListView(cosplayList))
  );
}