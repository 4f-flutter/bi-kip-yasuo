import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import '../../styles/texts.dart';
import '../../styles/colors.dart';

class OpenLinkListItem extends StatelessWidget {
  OpenLinkListItem({ this.title, this.itemUrl });

  final String title;
  final String itemUrl;

  _launchURL() async {
    var url = itemUrl;
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  Widget build(BuildContext context) => GestureDetector(
    child: Container(
      decoration: BoxDecoration(
        color: Color(0xFF082848),
      ),
      width: MediaQuery.of(context).size.width,
      margin: EdgeInsets.only(bottom: 10.0),
      padding: EdgeInsets.all(10.0),
      child: Text(title, style: textStyles['link']),
    ),
    onTap: _launchURL
  );
}

class OpenLinkList extends StatelessWidget {
  OpenLinkList({ this.items });

  final Widget items;

  Widget build(BuildContext context) => Container(
    color: colors['firefly'],
    child: items,
  );
}