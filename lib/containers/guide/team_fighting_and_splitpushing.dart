import 'package:flutter/material.dart';
import '../../styles/texts.dart';

class TeamfightingAndSplitpushing extends StatelessWidget {
  Widget build(BuildContext context) => Container(
    child: Column(
      children: <Widget>[
        Text(
          'GIAO TRANH VÀ ĐẨY LẺ',
          style: textStyles['heading'],
        ),
        Text(
          "Trong các cuộc giao tranh, bạn phải kiên nhẫn và để Bão Kiếm được cộng dồn. Khi bạn có 3 cộng dồn, bạn có thể tham gia tấn công nhiều kẻ thù bằng Trăn Trôi.\nTrước khi bạn sử dụng Trăn Trôi, hãy đảm bảo rằng đồng đội của bạn ở gần và sẵn sàng theo dõi. Không có gì đáng tin cậy hơn là hạ cánh sau khi Trăn Trối với 5 người, nhưng đồng đội của bạn không ở đây để được hỗ trợ, vì vậy bạn sẽ chết.\nNếu có ai đó có khả năng hất tung diện rộng như Malphite và Zac, tốt hơn là nên để họ vào đầu tiên.\nKhi chiến đấu, hãy đảm bảo rằng bạn chỉ chặn sát thương quan trọng và gây khống chế cứng với Tường Gió. Việc sử dụng đúng kĩ năng này có thể biến các trận đánh trở nên có lợi cho bạn.\nYasuo có những cách khác nhau để trở nên hữu ích trong một pha giao tranh, và tôi sẽ đề cập đến những cách phổ biến nhất ở đây.",
          style: textStyles['ability_detail'],
        ),
        Text(
          'Sát thủ',
          style: textStyles['neon_carrot_heading'],
        ),
        Text(
          "Bạn ám sát hậu tuyến của họ với một đồng đội khác, trước khi bạn ra đi. Ý tưởng ở đây là phá rồi hậu tuyến, và dọn dẹp những mục tiêu còn lại với đồng đội còn sống sót. This is a viable strategy when\n1. Bạn feed rất nhiều\n2. Bạn không thể dựa vào Marksman để gánh những trận đánh.\n3. Bạn có ai đó với khống chế cứng tin cậy để Trăn Trối.\n4. Hậu phương có thể gây ra một mối nguy lớn cho đồng đội của bạn. Ví dụ như một Varus đã feed.",
          style: textStyles['ability_detail'],
        ),
        Text(
          'Hỗ trợ',
          style: textStyles['neon_carrot_heading'],
        ),
        Text(
          "Bạn dính vào Marksman của bạn và tách họ khỏi bất cứ mối đe dọa nào đang ập đến. Đây là một chiến lược khả thi khi\n1. Bạn đứng sau vì bạn bị mất lane\n2. Marksman của bạn đang hoạt động tốt\n3. Kẻ địch có ai đó có thể xóa xổ Marksman của bạn ngay lập tức, ví dụ Kha'Zix, Zed, Syndra.\nChiến lược tốt nhất ở đây sẽ là hỗ trợ họ theo bất kỳ cách nào bạn có thể, để họ có thể thực hiện các trận đánh. Bạn không phải luôn là người gánh cả team khi chơi Yasuo. Đôi khi là người bảo vệ đồng đội của bạn thì tốt hơn.",
          style: textStyles['ability_detail'],
        ),
        Text(
          'Kẻ đánh nhau',
          style: textStyles['neon_carrot_heading'],
        ),
        Text(
          "Chiến lược này thường bao gồm các cuộc giao tranh nhỏ hơn dẫn đến các giao tranh lớn hơn. Bạn phối hợp với nhóm của bạn và chờ đợi thời điểm hoàn hảo để tham gia.",
          style: textStyles['ability_detail'],
        ),
        Text(
          'Đẩy lẻ là gì?',
          style: textStyles['neon_carrot_heading'],
        ),
        Text(
          "Đẩy lẻ là một chiến lược bạn có thể thực hiện khi:\n1. đội của bạn đang thi đấu không tốt\n2. bạn có một người đẩy lẻ thực sự mạnh mẽ có thể dễ dàng 1v1 bất cứ ai trong nhóm kẻ thù, và thậm chí có thể là 1v2.\nÝ tưởng là bạn có 4 người trong một làn đường, và sau đó bạn gửi người chia tách mạnh nhất đến một người bên ngoài, và để anh ta đẩy các mục tiêu trong khi đánh lạc hướng nhiều kẻ thù.\nNếu người đẩy lẻ bị toàn bộ đội địch truy đuổi, 4 người còn lại được tự do tiêu diệt các mục tiêu chính như baron / rồng / trụ và gây ức chế.\nNếu 4 thành viên đang giao tranhbởi toàn bộ đội quân địch, thì người đẩy lẻ có thể tự do phá hủy các trụ và thậm chí có thể là một kẻ quẩy rối.",
          style: textStyles['ability_detail'],
        ),
        Text(
          'Đẩy lẻ với Yasuo',
          style: textStyles['neon_carrot_heading'],
        ),
        Text(
          "Tôi có thể đẩy lẻ với Yasuo? Chính xác. Nhưng nó phải ở trong hoàn cảnh phù hợp (đã đề cập ở trên).\nBạn có thể chia nhỏ với Thiêu Đốt và Thanh Tẩy, nhưng Teleport là lựa chọn tốt nhất vì nó cho phép bạn giúp đội của mình, khi kẻ thù đang buộc một mục tiêu lớn như Baron.\nKhi bạn đẩy lẻ, bạn cần phải có đội của mình đẩy một làn đường khác. Họ không thể làm rừng hoặc chơi thụ động dưới trụ được.\nMục tiêu của bạn khi đẩy lẻ, là tạo áp lực và buộc nhiều kẻ thù đến và bảo vệ bạn. Khi bạn là kẻ chia rẽ mạnh nhất trong trò chơi, bạn có thể dễ dàng giết bất cứ ai trong 1v1, vì vậy kẻ thù sẽ hoảng loạn và gửi thêm người sau bạn.\nKhi bạn thấy mình trong tình huống 1v2 hoặc 1v3, bạn cần spam ping đồng đội của mình và bảo họ lấy trụ hoặc thậm chí là Baron.\nNếu nhóm của bạn đạt được mục tiêu lớn vì bạn đang đánh lạc hướng hơn 2 kẻ thù trên làn đường của mình, bạn đang làm đúng!\nĐẩy lẻ rất khó để thực hiện, bởi vì nó phụ thuộc vào đồng đội của bạn làm đúng việc vào đúng thời điểm. Tuy nhiên, nếu bạn định làm vậy, bạn sẽ thấy mình chiến thắng rất nhiều trận đấu, bởi vì kẻ địch không biết làm sao để đối phó. Đẩy lẻ như là một con dao 2 lưỡi vậy.",
          style: textStyles['ability_detail'],
        ),
      ],
    ),
  );
}