import 'package:flutter/material.dart';
import '../../styles/texts.dart';
import '../video.dart';

class AdvancedMechanics extends StatelessWidget {
  Widget build(BuildContext context) => Container(
    child: Column(
      children: <Widget>[
        Text(
          "KĨ THUẬT NÂNG CAO",
          style: textStyles['heading'],
        ),
        Text(
          "Yasuo có một vài kĩ thuật nâng cao, như di chuyển bất ngờ với Beyblade, hoặc Airblade để tối đa hoá DPS của bạn, và cho phép bạn tích công dồn lốc nhanh hơn.\nTôi sẽ sử dụng những clip dưới đây để cho bạn thấy làm sao thực hiện nó, bởi vì tôi thấy nó dễ hiễu hơn. Tôi được cho phép sử dụng 2 clip, được làm bởi ARKADATA. Tất cả credit thuộc về anh ấy!",
          style: textStyles['ability_detail'],
        ),
        Text(
          "Beyblade",
          style: textStyles['ability_name'],
        ),
        Video(src: 'assets/videos/beyblade.mp4'),
        Text(
          "Airblade",
          style: textStyles['ability_name'],
        ),
        Video(src: 'assets/videos/airblade.mp4'),
        Text(
          "Keyblade",
          style: textStyles['ability_name'],
        ),
        Video(src: 'assets/videos/keyblade.mp4'),
        Text(
          "Đây là combo khó nhất đối với Yasuo. Chúng ta sẽ gộp cả Beyblade và Airblade vào một. Video dưới đây được làm bởi TheWanderingPro",
          style: textStyles['ability_detail'],
        )
      ],
    ),
  );
}