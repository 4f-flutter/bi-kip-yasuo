import 'package:flutter/material.dart';
import '../../styles/texts.dart';

// Skill upgrade level
class OrderItem extends StatelessWidget {
  OrderItem({ this.number, this.color });

  final String number;
  final String color;

  @override
  Widget build(BuildContext context) => Container(
    padding: EdgeInsets.all(10.0),
    margin: EdgeInsets.only(right: 2.0),
    width: 40.0,
    height: 40.0,
    alignment: Alignment.center,
    color: Color(int.parse('0xFF' + color)),
    child: Text(number, textAlign: TextAlign.center, style: textStyles['ability_detail'],),
  );
}

// Each row of orders
class AbilityOrder extends StatelessWidget {
  AbilityOrder({ this.icon, this.orderList });

  final String icon;
  final List<int> orderList;

  Widget buildOrderItem(BuildContext bc, int index) {
      return orderList[index] == 0 ? OrderItem(number: '', color: '152034',) : OrderItem(number: (index + 1).toString(), color: '007236');
  }

  @override
  Widget build(BuildContext context) => Container(
    margin: EdgeInsets.only(bottom: 10.0),
    height: 60.0,
    child: ListView(
      scrollDirection: Axis.horizontal,
      children: <Widget>[
        Image.asset('assets/icons/skills/' + icon + '.webp'),
        ListView.builder(
          itemCount: orderList.length,
          // for each order in list if == 0 => none, if == 1 => index+1
          itemBuilder: (BuildContext bc, int i) => buildOrderItem(bc, i),
          shrinkWrap: true,
          scrollDirection: Axis.horizontal,
        ),
      ],
    ),
  );
}

// Orders
class AbilityOrders extends StatelessWidget {
  final steelTempest = [1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0];
  final windWall = [0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 1, 1];
  final spweepingBlade = [0, 1, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1, 1, 0, 0, 0, 0, 0];
  final lastBreath = [0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0];

  @override
  Widget build(BuildContext context) => Column(
    children: <Widget>[
      Text(
        "THỨ TỰ NÂNG KĨ NĂNG",
        style: textStyles['heading'],
      ),
      AbilityOrder(icon: 'q1', orderList: steelTempest),
      AbilityOrder(icon: 'w', orderList: windWall),
      AbilityOrder(icon: 'e', orderList: spweepingBlade),
      AbilityOrder(icon: 'r', orderList: lastBreath),
    ],
  );
}