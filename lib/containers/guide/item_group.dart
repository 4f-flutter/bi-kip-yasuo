import 'package:flutter/material.dart';
import '../../styles/texts.dart';

class Item extends StatelessWidget {
  Item({ this.img, this.name });

  final String img;
  final String name;

  Widget build(BuildContext context) => Container(
    padding: EdgeInsets.all(10.0),
    child: Center(
      child: Column(
        children: <Widget>[
          Image.asset('assets/icons/' + img, height: 50.0, width: 50.0,),
          Container(
            width: 100.0,
            padding: EdgeInsets.only(top: 10.0, bottom: 10.0),
            child: Text(name, textAlign: TextAlign.center, style: textStyles['item_name'],)
          ),
        ],
      ),
    ),
  );
}

class ItemGroup extends StatelessWidget {
  ItemGroup({ this.groupName, this.items });

  final String groupName;
  final Widget items;

  Widget build(BuildContext context) => Container(
    child: Column(
      children: <Widget>[
        Text(groupName, style: textStyles['item_group_name'],),
        Container(
          child: items,
          height: 130.0,
        ),
      ],
    ),
  );
}