import 'package:flutter/material.dart';
import '../guide/item_group.dart';
import '../../styles/texts.dart';

class RecommendItems extends StatelessWidget {
  final List<Item> startingItemsList = [
    Item(
      img: 'items/dorans-blade.gif',
      name: 'Kiếm Doran',
    ),
    Item(
      img: 'items/health-potion.gif',
      name: 'Bình Máu',
    ),
  ];

  final List<Item> alternativeStartList = [
    Item(
      img: 'items/dorans-shield.gif',
      name: 'Khiên Doran',
    ),
    Item(
      img: 'items/health-potion.gif',
      name: 'Bình Máu',
    ),
  ];

  final List<Item> coreItemsList = [
    Item(
      img: 'items/statikk-shiv.gif',
      name: 'Dao điện Statik',
    ),
    Item(
      img: 'items/infinity-edge.gif',
      name: 'Vô Cực Kiếm',
    ),
  ];

  final List<Item> alternativeToStatikList = [
    Item(
      img: 'items/phantom-dancer.gif',
      name: 'Vũ Điệu Tử Thần',
    ),
  ];

  final List<Item> bootsList = [
    Item(
      img: 'items/berserkers-greaves.gif',
      name: 'Giày Cuồng Nộ',
    ),
    Item(
      img: 'items/ninja-tabi.gif',
      name: 'Giày Ninja',
    ),
    Item(
      img: 'items/mercurys-treads.gif',
      name: 'Giày Thuỷ Ngân',
    ),
  ];

  final List<Item> postCoreItemsList = [
    Item(
      img: 'items/steraks-gage.gif',
      name: 'Móng Vuốt Steraks',
    ),
    Item(
      img: 'items/guardian-angel.gif',
      name: 'Giáp Thiên Thần',
    ),
    Item(
      img: 'items/deaths-dance.gif',
      name: 'Vũ Điệu Tử Thầnr',
    ),
  ];

  final List<Item> situationalItemsList = [
    Item(
      img: 'items/blade-of-the-ruined-king.gif',
      name: 'Gươm Vô Danh',
    ),
    Item(
      img: 'items/maw-of-malmortius.gif',
      name: 'Chuỳ Gai Malmortius',
    ),
    Item(
      img: 'items/the-bloodthirster.gif',
      name: 'Huyết Kiếm',
    ),
    Item(
      img: 'items/mercurial-scimitar.gif',
      name: 'Kiếm',
    ),
    Item(
      img: 'items/frozen-mallet.gif',
      name: 'Búa Băng',
    ),
    Item(
      img: 'items/mortal-reminder.gif',
      name: 'Lời Nhắc Tử Vong',
    ),
    Item(
      img: 'items/lord-dominiks-regards.gif',
      name: 'Nỏ Thần Dominiks',
    ),
  ];

  final List<Item> exampleBuild1 = [
    Item(
      img: 'items/berserkers-greaves.gif',
      name: 'Giày Cuồng Nộ ',
    ),
    Item(
      img: 'items/statikk-shiv.gif',
      name: 'Dao Điện Statikk',
    ),
    Item(
      img: 'items/infinity-edge.gif',
      name: 'Vô Cực Kiếm',
    ),
    Item(
      img: 'items/steraks-gage.gif',
      name: 'Móng vuốt Steraks',
    ),
    Item(
      img: 'items/guardian-angel.gif',
      name: 'Giáp Thiên Thần',
    ),
    Item(
      img: 'items/deaths-dance.gif',
      name: 'Vũ Điệu Tử Thần',
    ),
  ];

  final List<Item> exampleBuild2 = [
    Item(
      img: 'items/berserkers-greaves.gif',
      name: 'Giày Cuồng Nộ',
    ),
    Item(
      img: 'items/phantom-dancer.gif',
      name: 'Vũ Điệu Tử Thần',
    ),
    Item(
      img: 'items/infinity-edge.gif',
      name: 'Vô Cực Kiếm',
    ),
    Item(
      img: 'items/frozen-mallet.gif',
      name: 'Búa Băng',
    ),
    Item(
      img: 'items/guardian-angel.gif',
      name: 'Giáp Thiên Thần',
    ),
    Item(
      img: 'items/the-bloodthirster.gif',
      name: 'Huyết Kiếm',
    ),
  ];

  // Build ListView Items
  Widget buildItemsList(BuildContext context, int index, List<Item> list) => list[index];

  // Build ListView
  ListView buildListView(List<Item> list) => ListView.builder(
    itemCount: list.length,
    itemBuilder: (BuildContext bc, int i) => buildItemsList(bc, i, list),
    shrinkWrap: true,
    scrollDirection: Axis.horizontal,
  );

  Widget build(BuildContext context) => Container(
    child: Column(
      children: <Widget>[
        Text(
          "TRANG BỊ KHUYÊN DÙNG",
          style: textStyles['heading'], textAlign: TextAlign.center,
        ),
        ItemGroup(
          groupName: 'TRANG BỊ KHỞI ĐẦU',
          items: buildListView(startingItemsList)
        ),
        ItemGroup(
          groupName: 'DỰ PHÒNG',
          items: buildListView(alternativeStartList)
        ),
        ItemGroup(
          groupName: 'TRANG BỊ QUAN TRỌNG',
          items: buildListView(coreItemsList)
        ),
        ItemGroup(
          groupName: 'THAY THẾ DAO ĐIỆN STATIKK',
          items: buildListView(alternativeToStatikList)
        ),
        ItemGroup(
          groupName: 'GIÀY',
          items: buildListView(bootsList)
        ),
        ItemGroup(
          groupName: 'TRANG BỊ TRẤN PHÁI',
          items: buildListView(postCoreItemsList)
        ),
        ItemGroup(
          groupName: 'TRANG BỊ CUỐI TRẬN',
          items: buildListView(situationalItemsList)
        ),
        ItemGroup(
          groupName: 'NHÓM TRANG BỊ 1',
          items: buildListView(exampleBuild1)
        ),
        ItemGroup(
          groupName: 'NHÓM TRANG BỊ 2',
          items: buildListView(exampleBuild2)
        ),
      ],
    ),
  );
}