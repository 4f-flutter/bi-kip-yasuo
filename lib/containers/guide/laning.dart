import 'package:bi_kip_yasuo/styles/texts.dart';
import 'package:flutter/material.dart';
import '../../styles/texts.dart';

class Laning extends StatelessWidget {
  Widget build(BuildContext context) => Container(
    child: Column(
      children: <Widget>[
        Text(
          "ĐI ĐƯỜNG",
          style: textStyles['heading'],
        ),
        Text(
          "Trong giai đoạn đi đường, thay đổi lối chơi của bạn dựa trên kiểu tướng địch mà bạn đang đối đầu.",
          style: textStyles['ability_detail'],
        ),
        Text(
          "Chống lại tướng cận chiến",
          style: textStyles['neon_carrot_heading'],
        ),
        Text(
          "Yasuo có thể quẩy rối bất kì tướng cận chiến nào ở đầu trận. Nhiệm vụ của bạn ở đây là cấu rỉa hết sức có thể với Bão Kiếm, và giữ nó khi bạn tích được cộng dồn lốc.",
          style: textStyles['ability_detail'],
        ),
        Text(
          "Ví dụ 1",
          style: textStyles['neon_carrot_heading'],
        ),
        Text(
          "Hãy nói về việc bạn đang đi đường với Kassadin. Mục tiêu ở đây là từng con lính một. Cấu rỉa hắn với Bão Kiếm và đòn đánh cơ bản khi hắn đi tới để giết một con lính. Hắn có thể mất máu và có con lính, hoặc mất lính và chơi bị động. Bạn không cần phải lo từ khi lớp khiên nội tại đỡ được sát thương. Nếu hắn đỉnh đổi máu lấy lính, hắn sẽ thường xuyên ở trong khoảng hạ gục của bạn, từ khi chúng ta chơi với phép bổ trợ Thiêu Đốt. Một combo Beyblade có thể sử dụng để đảm bảo việc ăn mạng trước khi hắn kịp phản ứng.\nHoặc cách khác, nếu hắn chơi bị động - bạn sẽ có nhiều lính hơn, và vẫn thắng đường. Đó là tình huống luôn thắng cho bạn.\nBeyblade: xem ở phần \"KĨ THUẬT NÂNG CAO\" để biết thêm",
          style: textStyles['ability_detail'],
        ),
        Text(
          "Ví dụ 2",
          style: textStyles['neon_carrot_heading'],
        ),
        Text(
          "Bạn đang đi đường với Talon. Chiêu đẩy đường, cấu máu chủ yếu của hắn là Rake. Ở level 1 chúng ta sẽ cấu rỉa hắn với Bão Kiếm khi hắn tới để last hit. Nếu hắn chơi an toàn và last hit với Rake, đợt lính sẽ có xu hướng đẩy tới bạn, cho bạn tuỳ chọn đóng băng lính. Tại sao? Bởi Rake là kĩ năng diện rộng và sẽ gây sát thương lên cả những con lính khác. Khi hắn đạt level 2, chúng ta cần cẩn thận một chút, hắn có thể kích hoạt Sốc Điện và nội tại trong 1 combo nhanh. Thay vì trao đổi chiêu thức tệ như thế, chúng ta sẽ chơi thông minh và đứng ngoài tầm của Rake, và tìm một cơ hội trao đổi nhanh, để hắn không thể kích hoạt 2 thứ đó.\nTalon dựa vào đè đường và snowball. Nếu hắn không thể kiếm được một mạng ở đường, Hắn sẽ đẩy lính với Rake và đi roam. Khoảnh khắc hắn sử dụng Rake để đẩy đợt lính, chúng ta muốn lao vào và trừng phạt hắn khi kĩ năng chính của hắn đang trong thời gian hồi. Cách này sẽ giữ hắn ở lượng máu thấp, và khả năng ép hắn phải về bệ đá cổ thay vì đi roam.\nĐóng băng lính: Xem ở \"SỐNG SÓT QUA NHỮNG LẦN GANK VÀ KIỂM SOÁT LÍNH\" để biết thêm.",
          style: textStyles['ability_detail'],
        ),
        Text(
          "Đối đầu tướng tay dài",
          style: textStyles['neon_carrot_heading'],
        ),
        Text(
          "Đối với tướng tầm xa bạn nên chơi bị động tới khi bạn tìm ra cách. Hãy trở nên cẩn thận và farm nhiều nhất có thể, tới khi kẻ địch của bạn tạo sai lầm hoặc bạn ra ngoài.",
          style: textStyles['ability_detail'],
        ),
        Text(
          "Ví dụ 3",
          style: textStyles['neon_carrot_heading'],
        ),
        Text(
          "Bạn đang đi đường với Azir. Nếu đây là một người chơi Azir tốt, bạn sẽ cần chơi an toàn và farm lính với Bão Kiếm. Bạn không thể làm gì nhiều, khi hắn có lợi thế về tầm đánh, và hắn không phải đi bộ tới gần và đánh thường, bởi chiêu Arise của hắn!. Chiêu duy nhất là để hắn thay đổi vị trí của lính là Conquering Sands. Khi nó đang còn hồi, hắn sẽ có điểm yếu trong vài giây, và đó là lúc bạn có thể quẩy rối hắn. Emperor's Divide sẽ chỉ đẩy dối phương lại khi niệm và không còn ở trong khoảng cách của nó nữa, nên bạn có thể vượt qua nó. Nếu không có cách nào, lựa chọn duy nhất là đợi rừng và hỗ trợ gank.",
          style: textStyles['ability_detail'],
        ),
        Text(
          "Ví dụ 4",
          style: textStyles['neon_carrot_heading'],
        ),
        Text(
          "Bạn đang đi đường với Malzahar. Hắn được biết là một khắc chế cứng của Yasuo với chiêu không định hướng và khống chế cứng để tiêu diệt mọi tướng cơ động. Chúng ta có thể chơi trước khi hắn đạt level 6. Đa phần tôi chơi từ khi đạt level. Tôi sẽ cố giữ Void Shift của hắn trong thời gian hồi bằng cách lao lên thực hiện trao đổi ngắn. Bạn có thể thắng mọi lần trao đổi trước cấp 6 nếu bạn giết những Void Swarm của hắn nhanh. Sau level 6, lựa chọn tốt nhất là chơi an toàn tới khi bạn có Quicksilver Sash. Trong trận này, tôi mua Quicksilver Sash ngay sau những trang bị Zeal của tôi. Tại sao? Bởi tôi không muốn cho mạng hắn, và để đội tôi thụt lùi. Trước đó tôi không bận tâm nếu trang bị trấn phái của tôi bị chậm trễ một chút, nếu như tôi có thể an toàn, và để hắn lãng phí Nether Grasp.",
          style: textStyles['ability_detail'],
        ),
      ],
    ),
  );
}