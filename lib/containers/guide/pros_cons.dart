import 'package:flutter/material.dart';
import '../../styles/texts.dart';

class ProsCons extends StatelessWidget {
  Widget build(BuildContext context) => Container (
    child: Column(
      children: <Widget>[
        Text(
          "ĐIỂM MẠNH",
          style: textStyles['heading'],
        ),
        Text(
          '- Siêu cơ động\n- Rất xứng đáng khi thành thục\n- Khá tốt trong giao tranh và là tướng đẩy lẻ\n- Snowball cực mạnh khi có ưu thế\n- Kĩ năng hồi chiêu nhanh\n- Cực mạnh vào cuối trận\n- Không hao tốn tài nguyên',
          style: textStyles['ability_detail'],
          textAlign: TextAlign.justify,
        ),
        Text(
          "Yasuo là một lựa chọn rất tốt để solo queue với Quét Kiếm và tốt trong giao tranh với Trăn Trối. Nội tại cho hắn một lớp khiên miễn phí, và cho bạn gấp đôi chí mạng từ các trang bị, đó là lí do tại sao Ma Vũ Song Kiếm và Vô Cực Kiếm cực kì khoẻ. Tường Gió có thể ngăn chặn ADC / tướng tầm xa nếu đặt đúng chỗ. Bạn nên pick Yasuo khi bạn muốn một vị tướng có thể dựa vào khả năng solo để gánh team.",
          style: textStyles['ability_detail'],
          textAlign: TextAlign.justify,
        ),
        Text(
          "ĐIỂM YẾU",
          style: textStyles['heading'],
        ),
        Text(
          "- Pick trước có thể gây rủi ro\n- Khó để thành thục\n- Có điểm yếu về kiểm soát đám đông\n- Bị ban nhiều ở các bậc xếp hạng\n- Mọi người có xu hướng ghét người chơi Yasuo rất nhiều",
          style: textStyles['ability_detail'],
          textAlign: TextAlign.left,
        ),
        Text(
          "Yasuo thực sự rất yếu với chiêu không định hướng và khống chế như Nether Grasp, Rune Prison, Frenzying Taunt tới khi bạn có Quicksilver Sash. Hắn cũng có một số kẻ rất khó đối đầu như Pantheon, Ryze, Malzahar, vậy nếu pick bừa bãi có thể rất rủi ro, từ khi kẻ địch thường dựng một chiến thuật chỉ để chống lại bạn. Tường Gió có thời gian hồi chiêu rất lâu. Điểm yếu lớn nhất là có đồng đội của bạn cấm hắn.",
          style: textStyles['ability_detail'],
          textAlign: TextAlign.justify,
        ),
      ],
    ),
  );
}