import 'package:flutter/material.dart';
import '../guide/item_group.dart';
import '../../styles/texts.dart';

class RunesAndSpells extends StatelessWidget {
  final List<Item> primaryRunesList = [
    Item(
      img: 'runes/conqueror.png',
      name: 'Chinh Phục'
    ),
    Item(
      img: 'runes/overheal.png',
      name: 'Hồi Máu Vượt Trội'
    ),
    Item(
      img: 'runes/legend-alacrity.png',
      name: 'Huyền Thoại: Tốc Độ Đánh'
    ),
    Item(
      img: 'runes/coup-de-grace.png',
      name: 'Nhát Chém Ân Huệ',
    )
  ];

  final List<Item> secondaryRunesList = [
    Item(
      img: 'runes/taste-of-blood.png',
      name: 'Vị Máu'
    ),
    Item(
      img: 'runes/ravenous-hunter.png',
      name: 'Thợ Săn Tham Lam',
    )
  ];

  final List<Item> spellsList = [
    Item(
      img: 'spells/flash.png',
      name: 'Tốc Biến'
    ),
    Item(
      img: 'spells/ignite.png',
      name: 'Thiêu Đốt'
    ),
  ];

  // Build ListView Items
  Widget buildItemsList(BuildContext context, int index, List<Item> list) => list[index];

  // Build ListView
  ListView buildListView(List<Item> list) => ListView.builder(
    itemCount: list.length,
    itemBuilder: (BuildContext bc, int i) => buildItemsList(bc, i, list),
    shrinkWrap: true,
    scrollDirection: Axis.horizontal,
  );

  Widget build(BuildContext context) => Container(
    child: Column(
      children: <Widget>[
        Text(
          "NGỌC BỔ TRỢ VÀ PHÉP BỔ TRỢ",
          style: textStyles['heading'], textAlign: TextAlign.center
        ),
        ItemGroup(
          groupName: 'CHUẨN XÁC',
          items: buildListView(primaryRunesList)
        ),
        ItemGroup(
          groupName: 'ÁP ĐẢO',
          items: buildListView(secondaryRunesList)
        ),
        ItemGroup(
          groupName: 'PHÉP BỔ TRỢ',
          items: buildListView(spellsList)
        ),
      ],
    ),
  );
}