import 'package:flutter/material.dart';
import '../../styles/texts.dart';

class Ability extends StatelessWidget {
  Ability({ this.name, this.range, this.coolDown, this.icon, this.description });
  
  final String name;
  final String range;
  final String coolDown;
  final String icon;
  final Widget description;

  Widget build(BuildContext context) => Container(
    decoration: BoxDecoration(
      borderRadius: BorderRadius.circular(25.0),
      color: Color(0xFF082848),
    ),
    width: MediaQuery.of(context).size.width,
    padding: EdgeInsets.all(20),
    margin: EdgeInsets.only(top: 10.0, bottom: 10.0),
    child: Column(
      children: <Widget>[
        Row(
          children: <Widget>[
            Image.asset('assets/icons/skills/' + icon + '.webp'),
            Flexible(
              child: Column(
                children: <Widget>[
                  Text(name, textAlign: TextAlign.center, style: textStyles['ability_name'],),
                  Text.rich(
                    TextSpan(
                      text: 'TẦM SỬ DỤNG: ',
                      style: textStyles['ability_name'],
                      children: <TextSpan>[
                        TextSpan(text: range, style: textStyles['ability_detail']),
                      ],
                    ),
                  ),
                  Text.rich(
                    TextSpan(
                      text: 'THỜI GIAN HỒI: ',
                      style: textStyles['ability_name'],
                      children: <TextSpan>[
                        TextSpan(text: coolDown, style: textStyles['ability_detail']),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
        Padding(
          padding: EdgeInsets.only(top: 10.0, bottom: 10.0),
          child: description),
      ],
    ),
  );
}

class Abilities extends StatelessWidget {
  Widget build(BuildContext context) => Column(
    children: <Widget>[
      Text('BỘ KĨ NĂNG', style: textStyles['heading'],),
      Ability(
        name: 'Đạo Của Lãng Khách',
        range: '0',
        coolDown: '0',
        icon: 'passive',
        description: Column(
          children: <Widget>[
            Text('Ý Niệm: Tỉ lệ chí mạng của Yasuo tỉ lệ chí mạng được nhân đôi, nhưng chí mạng chỉ gây 90% damage, sát thương của Bão Kiếm bị giảm còn 75%.', textAlign: TextAlign.justify, style: textStyles['ability_detail'],),
            Text('Quyết Tâm: Yasuo được tăng 1% Nhịp mỗi khi di chuyển, di chuyển càng nhanh thì Nhịp hồi lại càng nhanh. Khi Nhịp đạt đến mức tối đa, sát thương từ một tướng hay quái sẽ khiến hắn nhận được một lớp lá chắn 100 - 510 (dựa trên số cấp độ) trong 1 giây', textAlign: TextAlign.justify, style: textStyles['ability_detail'],)
          ],
        ),
      ),
      Ability(
        name: 'Bão Kiếm',
        range: '475/900',
        coolDown: '4 - 1.33 (dựa vào tốc độ đánh)',
        icon: 'q1',
        description: Column(
          children: <Widget>[
            Text('Kích Hoạt: Yasuo đâm thẳng lưỡi kiếm về hướng chỉ định, gây sát thương vật lý lên những kẻ dính đòn. Bão Kiếm có thể chí mạng lên mục tiêu đầu tiên trúng phải.', textAlign: TextAlign.justify, style: textStyles['ability_detail'],),
            Text('Nếu Bão Kiếm được sử dụng trúng vào kẻ địch thì sẽ cho Yasuo một điểm cộng dồn Kiếm Theo Chiều Gió trong 10 giây. Khi đạt 2 điểm cộng dồn, Bão Kiếm thứ 3 sẽ tung ra một cơn lốc hất tung tất cả mục tiêu trên đường thẳng và mất đi toàn bộ điểm cộng dồn.', textAlign: TextAlign.justify, style: textStyles['ability_detail'],),
            Text('Bão Kiếm Cường Hóa: Bão Thép thứ 3 của Yasuo sẽ phóng ra một cơn lốc hất lên không tất cả kẻ địch trúng đòn.', textAlign: TextAlign.justify, style: textStyles['ability_detail'],),
            Text('Yasuo có thể sử dụng Bão Thép trong khi đang Quét Kiếm để thay đổi vùng ảnh hường thành hình tròn xung quanh, nhưng không thể hất tung thành đường thẳng khi có Bão Kiếm Cường Hóa. Thời gian hồi chiêu và niệm của Bão Thép được giảm bởi 0.598% cho mỗi 1% tốc độ đánh cộng thêm, lên đến 66.', textAlign: TextAlign.justify, style: textStyles['ability_detail'],)
          ],
        ),
      ),
      Ability(
        name: 'Tường Gió',
        range: '400',
        coolDown: '26 / 24 / 22 / 20 / 18',
        icon: 'w',
        description: Text('Kích Hoạt: Yasuo tạo ra một luồng gió mạnh thắng tiến thành dạng một bức tường trước hắn. Bức tường sẽ trôi nhẹ về trước 50 đơn vị trong 3.75 giây, chặn toàn bộ đạn đạo từ kẻ địch ngoại trừ đòn bắn từ trụ.', textAlign: TextAlign.justify, style: textStyles['ability_detail'],),
      ),
      Ability(
        name: 'Quét Kiếm',
        range: '475',
        coolDown: '0.5 / 0.4 / 0.3 / 0.2 / 0.1',
        icon: 'e',
        description: Column(
          children: <Widget>[
            Text('Kích Hoạt: Yasuo lướt một khoảng ngắn về hướng địch, gây sát thương phép phép và đánh dấu hồi chiêu lên mục tiêu. Tốc độ lướt của Yasuo được cộng thêm dựa trên tốc độ di chuyển.', style: textStyles['ability_detail'],),
            Text('Mỗi lần sử dụng chiêu, Yasuo được tăng 25% sát thương phép do lần dùng tiếp theo, cộng thêm lên đến 50%.', style: textStyles['ability_detail'],),
            Text('Yasuo không thể sử dụng Quét Kiếm lên mục tiêu còn bị đánh dấu.', textAlign: TextAlign.justify, style: textStyles['ability_detail'],),
          ],
        ),
      ),
      Ability(
        name: 'Trăn Trối',
        range: '1400',
        coolDown: '80 / 55 / 30',
        icon: 'r',
        description: Column(
          children: <Widget>[
            Text('Kích Hoạt: Yasuo lập tức dịch chuyển đến mục tiêu hiện diện gần nhất bị Hất Tung, lập tức hồi lại thanh Nhịp. Khi đến nơi, anh Áp Chế tất cả mục tiêu bị Hất Tung trong 1 giây và gây sát thương vật lý.', style: textStyles['ability_detail'],),
            Text('Trong 15s tiếp theo, nếu Yasuo gây Đòn Chí Mạng sẽ bỏ qua 50% giáp cộng thêm của mục tiêu. Sử dụng Trăn Trối sẽ bị mất các điểm cộng dồn của Bão Thép Cường Hóa.', textAlign: TextAlign.justify, style: textStyles['ability_detail'],)
          ],
        ),
      ),
    ],
  );
}