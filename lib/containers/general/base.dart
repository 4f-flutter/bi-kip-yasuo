import 'package:flutter/material.dart';
import '../../screens/short_story.dart';
import '../../styles/texts.dart';
import '../../styles/colors.dart';

class BaseInfo extends StatelessWidget {
  BaseInfo({ this.name, this.attribute });

  final String name;
  final String attribute;

  @override
  Widget build(BuildContext context) => Padding(
    padding: EdgeInsets.all(10.0),
    child: Text.rich(
      TextSpan(
        text: name+":\n",
        style: textStyles['khaki_heading'],
        children: <TextSpan>[
          TextSpan(
            text: attribute,
            style: textStyles['blue_text'],
          ),
        ],
      ),
    )
  );
}

class Base extends StatelessWidget {
  @override
  Widget build(BuildContext context) => Column(
    children: <Widget>[
      Text(
        'YASUO',
        style: textStyles['big_heading'],
      ),
      Text(
        'KẺ BẤT DUNG THỨ',
        style: textStyles['subtitle'],
      ),
      Padding(
        padding: EdgeInsets.only(top: 30.0, bottom: 30.0),
        child: CircleAvatar(
          backgroundImage: AssetImage('assets/images/yasuo-classic.jpg'),
          radius: 50.0,
        ),
      ),
      Padding(
        padding: EdgeInsets.only(top: 20.0, bottom: 20.0),
        child: Text(
          '"Cái chết như một cơn gió — luôn luôn bên cạnh ta."',
          textAlign: TextAlign.center,
          style: textStyles['quote']
        ),
      ),
      Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(25.0),
          color: colors['wood_smoke'],
        ),
        padding: EdgeInsets.all(20.0),
        child: Text(
          'Yasuo là một người Ionia kiên cường, một kiếm sĩ sử dụng chính không khí để đánh lại kẻ thù. Khi còn trẻ và kiêu hãnh, anh bị kết tội oan là đã sát hại sư phụ—không thể chứng tỏ sự vô tội, anh buộc phải xuống tay với chính anh trai để tự vệ. Kể cả sau khi hung thủ thực sự lộ diện, Yasuo vẫn không thể tha thứ cho bản thân vì những gì mình đã làm, và giờ lãng du khắp quê hương, với lưỡi kiếm chỉ có ngọn gió chỉ đường.',
          textAlign: TextAlign.justify ,
          style: textStyles['paragraph'],
        ),
      ),
      Container(
        margin: EdgeInsets.only(top: 20.0, bottom: 20.0),
        child: RaisedButton(
          color: Color.fromRGBO(25, 28, 33, 0.8),
          shape: Border.all(
            color: colors['cape_palliser'],
            width: 3.0,
          ),
          padding: EdgeInsets.all(20.0),
          child: Container(
            child: Text('Đọc tiểu sử', style: TextStyle(color: colors['cape_palliser'], fontSize: 18.0),),
          ),
          onPressed: () {
            Navigator.push(context, MaterialPageRoute(builder: (context) => ShortStory() ));
          }
        ),
      ),
      Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text('THÔNG TIN CƠ BẢN', style: textStyles['heading']),
          BaseInfo(name: 'GIÁ BÁN', attribute: "6300IP / 975RP"),
          Row(children: <Widget>[
            BaseInfo(name: 'NGÀY RA MẮT', attribute: "13-12-2013"),
            BaseInfo(name: 'THAY ĐỔI CUỐI', attribute: "V9.6"),
          ]),
          Row(children: <Widget>[
            BaseInfo(name: 'LOẠI TƯỚNG', attribute: "CẬN CHIẾN"),
            BaseInfo(name: 'THANH THỨ HAI', attribute: 'NHỊP'),
          ]),
          Text('CHỈ SỐ CƠ BẢN', style: textStyles['heading']),
          Row(children: <Widget>[
            BaseInfo(name: 'MÁU', attribute: "523 – 2002"),
            BaseInfo(name: 'TỐC ĐỘ HỒI MÁU', attribute: '6.5 – 21.8'),
          ]),
          BaseInfo(name: 'SÁT THƯƠNG CƠ BẢN', attribute: "60 – 114.4"),
          BaseInfo(name: 'TỐC ĐỘ ĐÁNH CƠ BẢN', attribute: '0.67 (+ 0 – 42.5%)(+ 0.027)'),
          Row(children: <Widget>[
            BaseInfo(name: 'GIÁP', attribute: "30 – 87.8"),
            BaseInfo(name: 'KHÁNG PHÉP', attribute: '30 – 51.3'),
          ]),
          Row(children: <Widget>[
            BaseInfo(name: 'TẦM ĐÁNH', attribute: "175"),
            BaseInfo(name: 'TỐC ĐỘ DI CHUYỂN', attribute: '345'),
          ]),
        ],
      ),
    ],
  );
}