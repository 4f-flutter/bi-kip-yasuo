import 'package:flutter/material.dart';
import '../../styles/texts.dart';
import '../../screens/skins/skin_details.dart';
import '../../styles/colors.dart';

class Skin extends StatelessWidget {
  Skin({ this.img });

  final String img;

  Widget build(BuildContext context) => Container(
    child: Image.asset('assets/images/yasuo-' + img + '.jpg'),
  );
}

class Skins extends StatelessWidget {
  @override
  Widget build(BuildContext context) => Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: <Widget>[
      Text('SKIN', style: textStyles['heading'],),
      Container(
        height: 250,
        padding: EdgeInsets.only(top: 10.0, bottom: 10.0),
        child: Stack(
          children: <Widget>[
            Skin(img: 'classic'),
            Align(
              alignment: Alignment.center,
              child: RaisedButton(
                color: Color.fromRGBO(25, 28, 33, 0.8),
                shape: Border.all(
                  color: colors['cape_palliser'],
                  width: 3.0,
                ),
                padding: EdgeInsets.all(20.0),
                child: Container(
                  child: Text('Xem thêm', style: TextStyle(color: colors['cape_palliser'], fontSize: 18.0)),
                ),
                onPressed: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context) => SkinDetails() ));
                },
              ),
            ),
          ],
        ),
      ),
    ],
  );
}