import 'package:flutter/material.dart';
import 'screens/home.dart';

void main() => runApp(BiKipYasuo());

class BiKipYasuo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Fashion Garden',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Home(),
    );
  }
}